#!/usr/bin/env python3

from ns1 import NS1
import requests
import os
import json

key = os.getenv('APIKEY', '')
record = os.getenv('RECORD', '')
webhook_url = os.getenv('WEBHOOK_URL', '')


def checkAndUpdate():
    api = NS1(apiKey=key)

    rec = api.loadRecord(record, 'A')

    current = rec.data['answers'][0]['answer'][0]

    my_ip = str(requests.get('https://ipv4.icanhazip.com').text).strip('\n')

    if current == my_ip:
        print("I match!")
        # exit(0)
    else:
        rec.update(answers=[my_ip])
        print("Updated IP")
        sendToSlack(current,my_ip)



def sendToSlack(old,new):
    payload = {
        'username': 'DDNS Updater',
        'text': "IP Has changed:\n *Old:* %s\n *New:* %s" % (old,new),
        'icon_emoji': ':ghost:'
        }
    if webhook_url != '':
        r = requests.post(webhook_url, data=json.dumps(payload), headers={'Content-Type': 'application/json'})
    else:
        print("No Slack for you")


if __name__ == '__main__':
    checkAndUpdate()
