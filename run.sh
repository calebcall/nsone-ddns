#!/bin/sh

if [[ -z "${SLEEP}" ]]; then
  SLEEP=300
fi

while :
do
  date
  echo "--- Start Update Process"

  python3 /cf/cf-ddns.py
  RET=$?
  if [ ${RET} -ne 0 ];
  then
    echo "Exit status not 0"
    echo "Sleep 120"
    sleep 120
  fi
  date
  echo "Sleep $SLEEP"
  sleep $SLEEP
done
