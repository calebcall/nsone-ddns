NSOne DDNS
======
Build Status:
------
[![pipeline status](https://gitlab.com/calebcall/nsone-ddns/badges/master/pipeline.svg)](https://gitlab.com/calebcall/nsone-ddns/commits/master)

Using this Image:
------
Running the container
A typical invocation of the container might be:

```
$ docker run -e APIKEY=8LOFcd6XCe5102SfFTcc \      
      -e RECORD=www.example.com \
      -e WEBHOOK_URL=https://hooks.slack.com/services/T85...
      -e SLEEP=300 \
      --name nsone-ddns \
      calebcall/nsone-ddns:latest
```

 > NSOne API key can be had by creating a new key at:
 >https://my.nsone.net/#/account/settings

The following environment variables can be used:
  - `APIKEY`: **Required** Set this to your API Key that you get from NS1 (**default:** `<blank>`)
  - `RECORD`: **Required** Set this to the dns record you want to update (**default:** `<blank>`)  
  - `WEBHOOK_URL`: **Optional** Set this to a slack webhook url if you want to be notified of any changes (**defaul:** `<blank>`)
  - `SLEEP`: Set this to the amount of time (in seconds) you want it to wait between polls (**default:** `300`)


Todo:
------
  - ~~Initial commit~~
  - Handle ipv6 in addition to ipv4

Prebuilt Docker image:
------
https://hub.docker.com/r/calebcall/nsone-ddns
