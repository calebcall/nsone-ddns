FROM alpine:latest

RUN apk add --update python3 py3-pip &&\
    mkdir /cf &&\
    pip3 install ns1-python requests &&\
    adduser -D cloudflare

COPY cf-ddns.py /cf/
COPY run.sh /cf/

RUN chmod +x /cf/*

USER cloudflare

CMD ["/cf/run.sh"]
